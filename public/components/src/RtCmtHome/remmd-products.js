import React from 'react'
import '../RtStylesheets/RtSS1.less'

export default class RemdProduct extends React.Component {
  constructor(props) {
    super(props)
  }

  render(){
    var Products = this.props.data.map(function(dt) {
      return (
        <div key={dt.id} className="RProduct-rd-single">
          <div className="RProduct-rd-half-t RProduct-rd-t-w">
            <img className="RProduct-rd-t-w" src={dt.picture} />
          </div>
          <div className="RProduct-rd-half-d">
            <h3 className="RProduct-rd-good-title">{dt.title}</h3>
            <div className="RProduct-rd-good-info">
              <em className="RProduct-product-show-price">
                <i>￥</i>{dt.price}
              </em>
            </div>
          </div>
        </div>
      )
    });
    return (
      <div>
        {Products}
      </div>
    )
  }
}