import React from 'react'
import '../RtStylesheets/RtSS1.less'

export default class LogoAlleyway extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    var LogoLists = this.props.data.map(function(logo) {
      return (
        <li key={logo.id} className="LoAy-Rtss">
          <a href={logo.link}><img src={logo.img} alt={logo.name} /></a>
        </li>
      )
    });

    return (
      <ul>
        {LogoLists}
      </ul>
    )
  }
}