var gulp = require('gulp'),
    less = require('gulp-less');
    //minifycss = require('gulp-minify-css');


gulp.task('less-compile', function() {
  gulp.src(['public/stylesheets/*.less'])
    .pipe(less())
    //.pipe(minifycss())
    .pipe(gulp.dest('public/build/v1'));
});


gulp.task('watch', function () {
  gulp.watch('public/stylesheets/*.less', ['less-compile']);
});

// gulp default task
gulp.task('default', ['watch']);
