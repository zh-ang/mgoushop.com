var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var cfg = require('./config');

module.exports = {
  entry: {
    components: './public/components/index.js',
    fetch: 'whatwg-fetch'
  },
  output: {
    path: path.resolve(cfg.APP_ROOT_PATH, "./public/build"),
    filename: 'v1/[name].bundle.js'
  },
  module: {
    loaders: [
      { test: require.resolve("react"), loader: "expose?React" },
      { test: /\.jsx?$/, loader: 'babel', exclude: /node_modules/ },
      { test: /\.less$/, loader: 'style-loader!css-loader!less-loader' }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      filename: 'v1/index.html',
      template: path.join(cfg.APP_ROOT_PATH, 'views/index.html')
    }),
    new HtmlWebpackPlugin({
      inject: true,
      filename: 'v1/index.html',
      template: path.join(cfg.APP_ROOT_PATH, 'views/index.html')
    })
  ],
  watch: true
};