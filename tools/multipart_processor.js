var formidable = require('formidable');
var form = new formidable.IncomingForm();
var cfg = require('../config');

form.uploadDir = cfg.APP_ROOT_PATH + '/upload';
form.keepExtensions = true;

module.exports = Processing;

function Processing(req) {
  form
    .on('error', processingErrors)
    .on('field')
    .on('file')
    .on('end', function () {
      
    });
  form.parse(req);
}


function processingErrors() {
  
}